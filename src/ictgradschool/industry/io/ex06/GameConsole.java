package ictgradschool.industry.io.ex06;




import ictgradschool.Keyboard;
import ictgradschool.industry.io.ex06.BullAndCow;

public class GameConsole {
    private BullAndCow bullandcow;

    /**
     * Construct a GameConsole object for starting the game.
     *
     * @param args The array of string parsed from command-line
     */
    public static void main(String[] args) {
        GameConsole gc = new GameConsole();
        gc.start();
    }


    /**
     * Constructor of GameConsole
     */
    public GameConsole() {
        bullandcow = new BullAndCow();
    }

    /**
     * Prints a welcome message, processes commands from the user, and
     * prints an exit message when the user quits.
     */
    public void start() {
        printWelcomeMessage();
        while (true) {
            // Get a command from the user and parse it
            String input = getCommand();
            if (input.equals("quit")) {
                break;
            }
            parseCommand(input);
        }
        printExitMessage();
    }

    /**
     * Prints the welcome message of the game.
     */
    private void printWelcomeMessage() {
        System.out.println("Welcome to Bull and Cow 2018!");
    }

    /**
     * Prints the exit message of the game.
     */
    private void printExitMessage() {
        System.out.println("Thanks for playing Bull and Cow 2018!");
    }

    /**
     * Gets a command from the user.
     *
     * @return A command from the user
     */
    private String getCommand() {
        System.out.print(">> ");
        return Keyboard.readInput();
    }


    private void parseCommand(String rawCmd) {
        String[] args = rawCmd.split(" ");

        // Empty commands can be ignored
        if (args.length == 0) {
            return;
        }

        String command = args[0];

        switch (command) {
            case "guess":
                guessCommand(args);
                break;

            default:
                System.out.println("Unrecognised command");
        }
    }

    private void guessCommand(String[] args) {
    }


}







