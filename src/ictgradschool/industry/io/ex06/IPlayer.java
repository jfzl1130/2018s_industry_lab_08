package ictgradschool.industry.io.ex06;

/**
 * An interface intended to be implemented by player, and can play game.
 */
public interface IPlayer {

    /** Returns four digits of guess
     */
    public int target();

    public boolean guessed();



}
