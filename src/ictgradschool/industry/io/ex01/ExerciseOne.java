package ictgradschool.industry.io.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        int word = 0;   // each word in the line

        String message = "";
        try (FileReader fR = new FileReader("input2.txt")) {

            while ((word = fR.read()) != -1) {
                total++;
                char character = (char) word;
                if (character == 'E' || character == 'e') {
                    numE++;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found problem" + e);
        } catch (IOException e) {
            System.out.println("IO problem" + e);
        }
        System.out.println(message);


        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;


        File myFile = new File("input2.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader("input2.txt"))){

            String line = null;
            while ((line = reader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    total++;

                    if (line.charAt(i) == 'E' || line.charAt(i) == 'e') {
                        numE++;

                    }
                }

            }
        }catch(IOException e){
                System.out.println("Error: " + e.getMessage());
            }


            // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
            // TODO Use a BufferedReader.

            System.out.println("Number of e/E's: " + numE + " out of " + total);
        }

        public static void main (String[]args){
            new ExerciseOne().start();
        }

    }

