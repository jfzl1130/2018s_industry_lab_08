package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter

        File myFile = new File(fileName);

        try (PrintWriter writer = new PrintWriter(new FileWriter(myFile))) {
            for (int i = 0; i < films.length; i++) {

                writer.printf("%s,%d,%d,%s%n", films[i].getName(), films[i].getYear(), films[i].getLengthInMinutes(), films[i].getDirector());
            }

        }catch(IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
