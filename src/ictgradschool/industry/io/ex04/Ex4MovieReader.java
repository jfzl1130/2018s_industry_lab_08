package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        File myFile = new File(fileName);
        Movie[] films= new Movie[19];


        try (Scanner scanner = new Scanner(myFile)) {
            int i= 0;
            scanner.useDelimiter(",|\r\n");
            while (scanner.hasNext()) {


                films[i]=new Movie(scanner.next(),scanner.nextInt(),scanner.nextInt(),scanner.next());
                i++;
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        // TODO Implement this with a Scanner

        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
